import React, { useState } from 'react'
import AddUserModal from '../models/AddUserModal';
import Button from 'react-bootstrap/Button';
function Home() {
    const [showModal,setShowModal]=useState(false);
    const handleCloseModal=()=>{
        setShowModal(false);
    }
    const handleOpenModal=()=>{
        setShowModal(true);
    }
    const getUserData=(data)=>{
console.log("Data from user modal:",data);
    }
  return (
    <div>
  <h5>Home Components</h5>
  <Button variant="primary" onClick={handleOpenModal}>Add User</Button>
  <AddUserModal show={showModal} handleClose={handleCloseModal} getUserData={getUserData}/>
    </div>
  )
}

export default Home
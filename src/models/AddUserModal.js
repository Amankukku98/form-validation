import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Validations from '../validations/Validation';
function AddUserModal({show,handleClose,getUserData}) {
//   const [show, setShow] = useState(false);
const [email,setEmail]=useState("");
const [error,setError]=useState('');

//   const handleClose = () => setShow(false);
//   const handleShow = () => setShow(true);
const handleCreateUser=()=>{
    if(email!==''){
      let validations=Validations.checkEmail(email);
      console.log(validations);
      if(validations){
        setError("Invalid Email!")
      }else{
        setError("");
        console.log("user created");
        getUserData(email);
      }
        // props.handleClose();
    }
}
  return (
    <>
      {/* <Button variant="primary" onClick={handleShow}>
        Add User
      </Button> */}

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Create User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="Enter Email" onChange={(event)=>setEmail(event.target.value)}/>
        {error && <div className='error'>{error}</div>}
      </Form.Group>
      {error && <div class="alert alert-danger" role="alert">{error}</div>}
    </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleCreateUser}>Create User</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default AddUserModal;
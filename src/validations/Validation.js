export default class Validations{
    static checkEmail(email){
     if(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)){
       return false;
     }else{
       return true;
     }
    }
    static checkPasswordLength(password){
     if(password.length<6){
        return false;
     }else{
        return true;
     }
    }
    static checkSalary(salary){
        if(salary){
            return false;
        }else{
            return true;
        }
    }
}